# -*- encoding: utf-8 -*-
# stub: opt_parse_validator 1.9.4 ruby lib

Gem::Specification.new do |s|
  s.name = "opt_parse_validator".freeze
  s.version = "1.9.4"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["WPScanTeam".freeze]
  s.date = "2021-03-22"
  s.description = "Implementation of validators for the ruby OptionParser lib. Mainly used in the CMSScanner gem to define the cli options available".freeze
  s.email = ["contact@wpscan.com".freeze]
  s.homepage = "https://github.com/wpscanteam/OptParseValidator".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.5".freeze)
  s.rubygems_version = "3.2.15".freeze
  s.summary = "Ruby OptionParser Validators".freeze

  s.installed_by_version = "3.2.15" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<activesupport>.freeze, [">= 5.2", "< 6.2.0"])
    s.add_runtime_dependency(%q<addressable>.freeze, [">= 2.5", "< 2.8"])
    s.add_development_dependency(%q<bundler>.freeze, [">= 1.6"])
    s.add_development_dependency(%q<rake>.freeze, ["~> 13.0"])
    s.add_development_dependency(%q<rspec>.freeze, ["~> 3.10.0"])
    s.add_development_dependency(%q<rspec-its>.freeze, ["~> 1.3.0"])
    s.add_development_dependency(%q<rubocop>.freeze, ["~> 1.11.0"])
    s.add_development_dependency(%q<rubocop-performance>.freeze, ["~> 1.10.0"])
    s.add_development_dependency(%q<simplecov>.freeze, ["~> 0.21.0"])
    s.add_development_dependency(%q<simplecov-lcov>.freeze, ["~> 0.8.0"])
  else
    s.add_dependency(%q<activesupport>.freeze, [">= 5.2", "< 6.2.0"])
    s.add_dependency(%q<addressable>.freeze, [">= 2.5", "< 2.8"])
    s.add_dependency(%q<bundler>.freeze, [">= 1.6"])
    s.add_dependency(%q<rake>.freeze, ["~> 13.0"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.10.0"])
    s.add_dependency(%q<rspec-its>.freeze, ["~> 1.3.0"])
    s.add_dependency(%q<rubocop>.freeze, ["~> 1.11.0"])
    s.add_dependency(%q<rubocop-performance>.freeze, ["~> 1.10.0"])
    s.add_dependency(%q<simplecov>.freeze, ["~> 0.21.0"])
    s.add_dependency(%q<simplecov-lcov>.freeze, ["~> 0.8.0"])
  end
end
