# -*- encoding: utf-8 -*-
# stub: wpscan 3.8.18 ruby lib

Gem::Specification.new do |s|
  s.name = "wpscan".freeze
  s.version = "3.8.18"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["WPScanTeam".freeze]
  s.date = "2021-06-08"
  s.description = "WPScan is a black box WordPress vulnerability scanner.".freeze
  s.email = ["contact@wpscan.com".freeze]
  s.executables = ["wpscan".freeze]
  s.files = ["bin/wpscan".freeze]
  s.homepage = "https://wpscan.com/wordpress-security-scanner".freeze
  s.licenses = ["Dual".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.5".freeze)
  s.rubygems_version = "3.2.15".freeze
  s.summary = "WPScan - WordPress Vulnerability Scanner".freeze

  s.installed_by_version = "3.2.15" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<cms_scanner>.freeze, ["~> 0.13.5"])
    s.add_development_dependency(%q<bundler>.freeze, [">= 1.6"])
    s.add_development_dependency(%q<memory_profiler>.freeze, ["~> 1.0.0"])
    s.add_development_dependency(%q<rake>.freeze, ["~> 13.0"])
    s.add_development_dependency(%q<rspec>.freeze, ["~> 3.10.0"])
    s.add_development_dependency(%q<rspec-its>.freeze, ["~> 1.3.0"])
    s.add_development_dependency(%q<rubocop>.freeze, ["~> 1.16.0"])
    s.add_development_dependency(%q<rubocop-performance>.freeze, ["~> 1.11.0"])
    s.add_development_dependency(%q<simplecov>.freeze, ["~> 0.21.0"])
    s.add_development_dependency(%q<simplecov-lcov>.freeze, ["~> 0.8.0"])
    s.add_development_dependency(%q<stackprof>.freeze, ["~> 0.2.12"])
    s.add_development_dependency(%q<webmock>.freeze, ["~> 3.13.0"])
  else
    s.add_dependency(%q<cms_scanner>.freeze, ["~> 0.13.5"])
    s.add_dependency(%q<bundler>.freeze, [">= 1.6"])
    s.add_dependency(%q<memory_profiler>.freeze, ["~> 1.0.0"])
    s.add_dependency(%q<rake>.freeze, ["~> 13.0"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.10.0"])
    s.add_dependency(%q<rspec-its>.freeze, ["~> 1.3.0"])
    s.add_dependency(%q<rubocop>.freeze, ["~> 1.16.0"])
    s.add_dependency(%q<rubocop-performance>.freeze, ["~> 1.11.0"])
    s.add_dependency(%q<simplecov>.freeze, ["~> 0.21.0"])
    s.add_dependency(%q<simplecov-lcov>.freeze, ["~> 0.8.0"])
    s.add_dependency(%q<stackprof>.freeze, ["~> 0.2.12"])
    s.add_dependency(%q<webmock>.freeze, ["~> 3.13.0"])
  end
end
