# -*- encoding: utf-8 -*-
# stub: cms_scanner 0.13.5 ruby lib

Gem::Specification.new do |s|
  s.name = "cms_scanner".freeze
  s.version = "0.13.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["WPScanTeam".freeze]
  s.date = "2021-06-08"
  s.description = "Framework to provide an easy way to implement CMS Scanners".freeze
  s.email = ["contact@wpscan.com".freeze]
  s.homepage = "https://github.com/wpscanteam/CMSScanner".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.5".freeze)
  s.rubygems_version = "3.2.15".freeze
  s.summary = "CMS Scanner Framework (experimental)".freeze

  s.installed_by_version = "3.2.15" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_runtime_dependency(%q<get_process_mem>.freeze, ["~> 0.2.5"])
    s.add_runtime_dependency(%q<nokogiri>.freeze, ["~> 1.11.4"])
    s.add_runtime_dependency(%q<opt_parse_validator>.freeze, ["~> 1.9.4"])
    s.add_runtime_dependency(%q<public_suffix>.freeze, ["~> 4.0.3"])
    s.add_runtime_dependency(%q<ruby-progressbar>.freeze, [">= 1.10", "< 1.12"])
    s.add_runtime_dependency(%q<typhoeus>.freeze, [">= 1.3", "< 1.5"])
    s.add_runtime_dependency(%q<ethon>.freeze, ["~> 0.14.0"])
    s.add_runtime_dependency(%q<xmlrpc>.freeze, ["~> 0.3"])
    s.add_runtime_dependency(%q<yajl-ruby>.freeze, ["~> 1.4.1"])
    s.add_runtime_dependency(%q<sys-proctable>.freeze, ["~> 1.2.2"])
    s.add_development_dependency(%q<bundler>.freeze, [">= 1.6"])
    s.add_development_dependency(%q<rake>.freeze, ["~> 13.0"])
    s.add_development_dependency(%q<rspec>.freeze, ["~> 3.10.0"])
    s.add_development_dependency(%q<rspec-its>.freeze, ["~> 1.3.0"])
    s.add_development_dependency(%q<rubocop>.freeze, ["~> 1.16.0"])
    s.add_development_dependency(%q<rubocop-performance>.freeze, ["~> 1.11.0"])
    s.add_development_dependency(%q<simplecov>.freeze, ["~> 0.21.0"])
    s.add_development_dependency(%q<simplecov-lcov>.freeze, ["~> 0.8.0"])
    s.add_development_dependency(%q<webmock>.freeze, ["~> 3.13.0"])
  else
    s.add_dependency(%q<get_process_mem>.freeze, ["~> 0.2.5"])
    s.add_dependency(%q<nokogiri>.freeze, ["~> 1.11.4"])
    s.add_dependency(%q<opt_parse_validator>.freeze, ["~> 1.9.4"])
    s.add_dependency(%q<public_suffix>.freeze, ["~> 4.0.3"])
    s.add_dependency(%q<ruby-progressbar>.freeze, [">= 1.10", "< 1.12"])
    s.add_dependency(%q<typhoeus>.freeze, [">= 1.3", "< 1.5"])
    s.add_dependency(%q<ethon>.freeze, ["~> 0.14.0"])
    s.add_dependency(%q<xmlrpc>.freeze, ["~> 0.3"])
    s.add_dependency(%q<yajl-ruby>.freeze, ["~> 1.4.1"])
    s.add_dependency(%q<sys-proctable>.freeze, ["~> 1.2.2"])
    s.add_dependency(%q<bundler>.freeze, [">= 1.6"])
    s.add_dependency(%q<rake>.freeze, ["~> 13.0"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.10.0"])
    s.add_dependency(%q<rspec-its>.freeze, ["~> 1.3.0"])
    s.add_dependency(%q<rubocop>.freeze, ["~> 1.16.0"])
    s.add_dependency(%q<rubocop-performance>.freeze, ["~> 1.11.0"])
    s.add_dependency(%q<simplecov>.freeze, ["~> 0.21.0"])
    s.add_dependency(%q<simplecov-lcov>.freeze, ["~> 0.8.0"])
    s.add_dependency(%q<webmock>.freeze, ["~> 3.13.0"])
  end
end
