# From here https://github.com/fish-shell/fish-shell/issues/1403#issuecomment-397109051
function fish_vi_cursor --on-variable fish_bind_mode
    if set -q __last_fish_bind_mode
        and test $__last_fish_bind_mode = $fish_bind_mode
        return
    end
    set -g __last_fish_bind_mode $fish_bind_mode
    switch $fish_bind_mode
        case insert
            printf '\e]50;CursorShape=1\x7'
        case default
            printf '\e]50;CursorShape=0\x7'
        case "*"
            printf '\e]50;CursorShape=0\x7'
    end
end

function cat
  bat --paging=never $argv
end

set _CONDA_ROOT "/Users/frsr/.local/miniforge3"

alias bd '/opt/homebrew/bin/brew bundle dump'
alias brew 'doas -u a -- brew'
alias b 'doas -u a -- brew'
alias bi 'doas -u a -- brew install'
alias bs 'doas -u a -- brew search'
alias bu 'doas -u a -- brew upgrade'
alias bb 'doas -u a -- brew cleanup && doas -u a -- brew doctor'
alias wpscan 'wpscan --api-token V2i0PzehXCKJN1y0miJhOVizb4UrYmUOSdZiB2aXdQU --url'
alias wps 'wpscan --api-token V2i0PzehXCKJN1y0miJhOVizb4UrYmUOSdZiB2aXdQU --url'

function b64d
    pbpaste | base64 -d | pbcopy
end

function fish_mode_prompt
end

test -e $HOME/.config/fish/iterm2_shell_integration.fish ; and source $HOME/.config/fish/iterm2_shell_integration.fish

# test -e $HOME/.iterm2_shell_integration.fish ; and source $HOME/.iterm2_shell_integration.fish

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
eval /Users/frsr/.local/miniforge3/bin/conda "shell.fish" "hook" $argv | source
# <<< conda initialize <<<

# test -e $_CONDA_ROOT/etc/profile.d/conda.sh; and conda activate "$argv"

starship init fish | source
zoxide init fish | source
status --is-interactive; and source (rbenv init -|psub)
